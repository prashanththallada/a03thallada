# A03 

A simple web applications using Node, Express, BootStrap, EJS, HTML, CSS

## How to use

Open a command window in your c:\44563\s123456\a03Thallada folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8999`. 
